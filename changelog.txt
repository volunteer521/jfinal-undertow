jfinal undertow 1.5:
1：对 jfinal 的依赖改为 provided，方便未来独立升级 jfinal 版本
2：ResourceManagerKit 更名为 ResourceManagerBuilder
3：ResourceManagerBuilder 支持 undertow.resourcePath=classpath:static 形式的配置，用于指定从 classpath 或 jar 包内的目录加载 web 资源
4：增强 ResourceManagerBuilder，支持 visual studio code 的大小写问题，优化性能
5：改进 session 持久化模块，解决多次连续 hot swap 之间未触摸 session 时无法保持住 session 的问题
6：undertow 由 2.0.16.Final 升级到 2.0.17.Final

---------------------------------------------------------------------------------------

jfinal undertow 1.4:
1：改进 session 持久化功能，解决 model 类型转换异常问题
2：改进 UndertowServer，让 URL 支持特殊字符，便于使用问号挂参传递 json 等类型数据
3：UndertowServer 中对于 configConsumer.accept(...) 的调用前移，支持更多 lambda 配置
4：http 重定向到 https 支持状态码，默认状态码为 302，可通过 undertow.http.toHttpsStatusCode 配置
5：UndertowServer 输出的 host 信息中添加 "http://"，便于在 IDEA 中点击打开浏览器
6：重构 UndertowConfig，抽取 createPropExt(...) 方法
7：PropExt 添加带 Properties 的构造方法



